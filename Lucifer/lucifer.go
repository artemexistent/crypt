package main

import "flag"
import "fmt"
import "io"
import "os"

var SBoxZero = [16]byte{12, 15, 7, 10, 14, 13, 11, 0, 2, 6, 3, 1, 9, 4, 5, 8}
var SBoxOne = [16]byte{7, 2, 14, 9, 3, 11, 0, 4, 12, 13, 1, 10, 6, 15, 8, 5}

var BytePermute = [256]byte{
	0, 2, 1, 3, 64, 66, 65, 67,
	32, 34, 33, 35, 96, 98, 97, 99,
	8, 10, 9, 11, 72, 74, 73, 75,
	40, 42, 41, 43, 104, 106, 105, 107,
	128, 130, 129, 131, 192, 194, 193, 195,
	160, 162, 161, 163, 224, 226, 225, 227,
	136, 138, 137, 139, 200, 202, 201, 203,
	168, 170, 169, 171, 232, 234, 233, 235,
	4, 6, 5, 7, 68, 70, 69, 71,
	36, 38, 37, 39, 100, 102, 101, 103,
	12, 14, 13, 15, 76, 78, 77, 79,
	44, 46, 45, 47, 108, 110, 109, 111,
	132, 134, 133, 135, 196, 198, 197, 199,
	164, 166, 165, 167, 228, 230, 229, 231,
	140, 142, 141, 143, 204, 206, 205, 207,
	172, 174, 173, 175, 236, 238, 237, 239,
	16, 18, 17, 19, 80, 82, 81, 83,
	48, 50, 49, 51, 112, 114, 113, 115,
	24, 26, 25, 27, 88, 90, 89, 91,
	56, 58, 57, 59, 120, 122, 121, 123,
	144, 146, 145, 147, 208, 210, 209, 211,
	176, 178, 177, 179, 240, 242, 241, 243,
	152, 154, 153, 155, 216, 218, 217, 219,
	184, 186, 185, 187, 248, 250, 249, 251,
	20, 22, 21, 23, 84, 86, 85, 87,
	52, 54, 53, 55, 116, 118, 117, 119,
	28, 30, 29, 31, 92, 94, 93, 95,
	60, 62, 61, 63, 124, 126, 125, 127,
	148, 150, 149, 151, 212, 214, 213, 215,
	180, 182, 181, 183, 244, 246, 245, 247,
	156, 158, 157, 159, 220, 222, 221, 223,
	188, 190, 189, 191, 252, 254, 253, 255,
}

var ByteReverse = [256]byte{
	0, 128, 64, 192, 32, 160, 96, 224,
	16, 144, 80, 208, 48, 176, 112, 240,
	8, 136, 72, 200, 40, 168, 104, 232,
	24, 152, 88, 216, 56, 184, 120, 248,
	4, 132, 68, 196, 36, 164, 100, 228,
	20, 148, 84, 212, 52, 180, 116, 244,
	12, 140, 76, 204, 44, 172, 108, 236,
	28, 156, 92, 220, 60, 188, 124, 252,
	2, 130, 66, 194, 34, 162, 98, 226,
	18, 146, 82, 210, 50, 178, 114, 242,
	10, 138, 74, 202, 42, 170, 106, 234,
	26, 154, 90, 218, 58, 186, 122, 250,
	6, 134, 70, 198, 38, 166, 102, 230,
	22, 150, 86, 214, 54, 182, 118, 246,
	14, 142, 78, 206, 46, 174, 110, 238,
	30, 158, 94, 222, 62, 190, 126, 254,
	1, 129, 65, 193, 33, 161, 97, 225,
	17, 145, 81, 209, 49, 177, 113, 241,
	9, 137, 73, 201, 41, 169, 105, 233,
	25, 153, 89, 217, 57, 185, 121, 249,
	5, 133, 69, 197, 37, 165, 101, 229,
	21, 149, 85, 213, 53, 181, 117, 245,
	13, 141, 77, 205, 45, 173, 109, 237,
	29, 157, 93, 221, 61, 189, 125, 253,
	3, 131, 67, 195, 35, 163, 99, 227,
	19, 147, 83, 211, 51, 179, 115, 243,
	11, 139, 75, 203, 43, 171, 107, 235,
	27, 155, 91, 219, 59, 187, 123, 251,
	7, 135, 71, 199, 39, 167, 103, 231,
	23, 151, 87, 215, 55, 183, 119, 247,
	15, 143, 79, 207, 47, 175, 111, 239,
	31, 159, 95, 223, 63, 191, 127, 255,
}

func reverseByte(n byte) (reversed byte) {
	return ByteReverse[n]
}

func applySBoxes(msg byte, icb bool) (confused byte) {
	var leftbits byte = 0
	var rightbits byte = 0

	for i := uint(4); i <= 7; i++ {
		leftbits = (leftbits * 2) + (msg >> i & 0x01)
	}

	for i := uint(0); i <= 3; i++ {
		rightbits = (rightbits * 2) + (msg >> i & 0x01)
	}

	if icb {
		return reverseByte((SBoxOne[rightbits] << 4) | SBoxZero[leftbits])
	} else {
		return reverseByte((SBoxOne[leftbits] << 4) | SBoxZero[rightbits])
	}
}

func applyPermutation(interrupted byte) (permuted byte) {
	return BytePermute[interrupted]
}

func applyDiffusion(permuted byte, offset uint8, msgHalf []byte) {
	if ((permuted >> 7) & 0x01) == 0x01 {
		msgHalf[(7+offset)%8] ^= 128
	}
	if ((permuted >> 6) & 0x01) == 0x01 {
		msgHalf[(6+offset)%8] ^= 64
	}
	if ((permuted >> 5) & 0x01) == 0x01 {
		msgHalf[(2+offset)%8] ^= 32
	}
	if ((permuted >> 4) & 0x01) == 0x01 {
		msgHalf[(1+offset)%8] ^= 16
	}
	if ((permuted >> 3) & 0x01) == 0x01 {
		msgHalf[(5+offset)%8] ^= 8
	}
	if ((permuted >> 2) & 0x01) == 0x01 {
		msgHalf[(0+offset)%8] ^= 4
	}
	if ((permuted >> 1) & 0x01) == 0x01 {
		msgHalf[(3+offset)%8] ^= 2
	}
	if ((permuted >> 0) & 0x01) == 0x01 {
		msgHalf[(4+offset)%8] ^= 1
	}
}

func stepfn(keyByte byte, upperMsgByte byte, icb bool, stepNum uint8, lowerMsgHalf []byte) {
	var confusedByte = applySBoxes(upperMsgByte, icb)
	var interruptedByte = confusedByte ^ keyByte
	var permutedByte = applyPermutation(interruptedByte)
	applyDiffusion(permutedByte, stepNum, lowerMsgHalf)
}

func encryptBlock(key []byte, msg []byte) {
	var msgLowerHalf = msg[0:8]
	var msgUpperHalf = msg[8:16]

	for round := uint8(0); round < 16; round++ {
		var transformControlByte = key[(round*7)%16]

		for step := uint8(0); step < 8; step++ {
			var keyByte = key[((round*7)+step)%16]
			var upperMsgByte = msgUpperHalf[step]
			var icb = ((transformControlByte >> (7 - step)) & 0x01) == 0x01
			stepfn(keyByte, upperMsgByte, icb, step, msgLowerHalf)
		}

		msgLowerHalf, msgUpperHalf = msgUpperHalf, msgLowerHalf
	}

	for i := 0; i < 8; i++ {
		msgLowerHalf[i], msgUpperHalf[i] = msgUpperHalf[i], msgLowerHalf[i]
	}
}

func decryptBlock(key []byte, msg []byte) {
	var msgLowerHalf = msg[0:8]
	var msgUpperHalf = msg[8:16]

	for round := uint8(0); round < 16; round++ {
		var transformControlByte = key[((round+1)*9)%16]

		for step := uint8(0); step < 8; step++ {
			var keyByte = key[(((round+1)*9)+step)%16]
			var upperMsgByte = msgUpperHalf[step]
			var icb = ((transformControlByte >> (7 - step)) & 0x01) == 0x01
			stepfn(keyByte, upperMsgByte, icb, step, msgLowerHalf)
		}

		msgLowerHalf, msgUpperHalf = msgUpperHalf, msgLowerHalf
	}

	for i := 0; i < 8; i++ {
		msgLowerHalf[i], msgUpperHalf[i] = msgUpperHalf[i], msgLowerHalf[i]
	}
}

func processFile(keyFile, inputFile, outputFile string, encrypt bool) {
	key := make([]byte, 16)
	fKey, err := os.Open(keyFile)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer fKey.Close()
	nBytesKey, err := fKey.Read(key)
	if err != nil {
		fmt.Println(err)
		return
	}
	if nBytesKey != 16 {
		fmt.Println("key_file less than 16 bytes long")
		return
	}

	fInput, err := os.Open(inputFile)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer fInput.Close()
	f_output, err := os.Create(outputFile)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f_output.Close()

	inputBlock := make([]byte, 16)
	var paddingAdded = false
	for {
		nBytesInput, err := fInput.Read(inputBlock)
		if err != nil {
			if err == io.EOF {
				if paddingAdded == false && encrypt == true {
					var padBlock = []byte{
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
						0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10,
					}
					encryptBlock(key, padBlock)
					f_output.Write(padBlock)
				}
				if encrypt == false {
					fileInfo, err := f_output.Stat()
					if err != nil {
						fmt.Println("couldn't get output file stat")
						return
					}
					file_length := fileInfo.Size()

					pad_len_arr := make([]byte, 1)
					f_output.ReadAt(pad_len_arr, file_length-1)
					padding_length := pad_len_arr[0]

					f_output.Truncate(file_length - int64(padding_length))
				}
				return
			}

			fmt.Println(err)
			return
		}
		if nBytesInput < 16 {
			if encrypt == true {
				for i := nBytesInput; i < 15; i++ {
					inputBlock[i] = 0x00
				}
				//last byte = number of padding bytes
				inputBlock[15] = byte(16 - nBytesInput)
				paddingAdded = true
			} else {
				fmt.Println("Number of bytes in file to decrypt not divisible by 16")
				return
			}
		}

		if encrypt == true {
			encryptBlock(key, inputBlock)
		} else {
			decryptBlock(key, inputBlock)
		}

		f_output.Write(inputBlock)
	}
}

func printUsage() {
	fmt.Println("Usage: lucifer-go (--encrypt | --decrypt) <keyfile> <inputfile> <outputfile>")
}

func main() {
	encrypt := flag.Bool("encrypt", false, "encrypt a file")
	decrypt := flag.Bool("decrypt", false, "decrypt a file")
	flag.Parse()
	if len(flag.Args()) != 3 {
		fmt.Println("Unrecognized number of arguments")
		printUsage()
		return
	}
	if *encrypt && *decrypt {
		fmt.Println("You can either encrypt or decrypt, not both")
		printUsage()
		return
	}
	if !*encrypt && !*decrypt {
		fmt.Println("Please provide an --encrypt or a --decrypt option")
		printUsage()
		return
	}

	keyfile := flag.Args()[0]
	inputfile := flag.Args()[1]
	outputfile := flag.Args()[2]

	processFile(keyfile, inputfile, outputfile, *encrypt)
}
